#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------


# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve3(self):
        r = StringIO("A Paris Hold\nB Austin Move Paris")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve4(self):
        r = StringIO("A Paris Move Austin\nB Austin Move Paris")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB Paris\n")

    def test_solve5(self):
        r = StringIO("A Shanghai Move Tokyo\nD Tokyo Hold\nP Moscow Move Tokyo\nS Cairo Hold\nW Seoul Support A\nZ CapeTown Support P")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nD [dead]\nP [dead]\nS Cairo\nW Seoul\nZ CapeTown\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

#need to update this with our output
""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py > TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.....
----------------------------------------------------------------------
Ran 5 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          89     20     58      5    78%   76-77, 83, 101, 142-153, 165-182, 41->44, 75->76, 81->83, 98->101, 124->142
TestDiplomacy.py      31      0      0      0   100%
--------------------------------------------------------------
TOTAL                120     20     58      5    81%

"""
