#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------


from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"
        i = diplomacy_read(s)
        self.assertEqual(i,  "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        self.assertEqual(v, "A [dead]\nB Madrid\nC London\n")


    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        diplomacy_print(w, "A [dead]\nB Madrid\nC London\n")
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve3(self):
        r = StringIO("A Houston Hold\nB Toronto Move Houston\nC London Support B\nD Austin Move London\nE Seattle Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD [dead]\nE Seattle\n")


# ----
# main
# ----
if __name__ == "__main__":
    main()
