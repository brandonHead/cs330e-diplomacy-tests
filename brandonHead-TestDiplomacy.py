#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold"
        i = diplomacy_read(s)
        self.assertEqual(i,  ["A", "Madrid", "Hold"])

    def test_read2(self):
        s = "B Dallas Move Austin"
        i = diplomacy_read(s)
        self.assertEqual(i, ["B", "Dallas", "Move", "Austin"])

    def test_read3(self):
        s = "C Budapest Support A"
        i = diplomacy_read(s)
        self.assertEqual(i, ["C", "Budapest", "Support", "A"])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(["A Madrid Hold"])
        self.assertEqual(v, "A Madrid\n")

    def test_eval_2(self):
        v = diplomacy_eval(["A Madrid Hold", "B London Move Madrid"])
        self.assertEqual(v, "A [dead]\nB [dead]\n")

    def test_eval_3(self):
        v = diplomacy_eval(["A Madrid Hold", "B London Move Madrid", "C Vienna Support A"])
        self.assertEqual(v, "A Madrid\nB [dead]\nC Vienna\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Munich Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC Munich\n\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()
